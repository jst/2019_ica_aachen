# The open-source Virtual Acoustics (VA) real-time auralization framework

Resources for the poster on **The open-source Virtual Acoustics (VA) real-time auralization framework** presented at the [International Conference on Acoustics (ICA), 2019 in Aachen, Germany](http://www.ica2019.org).


### PDF 

The [final poster](https://git.rwth-aachen.de/jst/2019_ica_aachen/blob/master/stienen2019virtual.pdf) is available in **PDF format**. 

### Resources

Resources are of various types (PDF, SVG, PNG) and can be found in the **images** folder. The poster layout application was Adobe InDesign, but the project files could not be provided due to copyright issues concerning the template.
The most important two images used are the [VA circular diagram](https://git.rwth-aachen.de/jst/2019_ica_aachen/blob/master/images/va_circular_diagram.png) and the [DSP circular diagram](https://git.rwth-aachen.de/jst/2019_ica_aachen/blob/master/images/dsp_circular_diagram.png), which are added as PNG's for convenience. Find the vector-based graphic files in the *images subfolder*.

### Copyright

The poster text, the circular diagrams and the screenshot images from Redstart and VAServer are provided under CC BY-ND 4. Other high resolution pictures may show logos and trademarks that are copyright protected and are not shown clearly or readable in size in the printed poster resolution. The logos from the conference and universities have been used with permission and may only be used by others with explicit permission of the copyright holder.
